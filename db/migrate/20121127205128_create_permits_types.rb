class CreatePermitsTypes < ActiveRecord::Migration
  def change
    create_table :permits_types, :id => false do |t|
      t.integer :permit_id
      t.integer :type_id
    end
  end
end
