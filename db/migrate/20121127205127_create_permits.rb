class CreatePermits < ActiveRecord::Migration
  def change
    create_table :permits do |t|
      t.string :name
      t.string :description
      t.string :string
      t.string :url
      t.string :county
      t.string :state
      t.string :city
      t.integer :section
      t.text :resource_group_description

      t.timestamps
    end
  end
end
