class AddCategoryIdToTypes < ActiveRecord::Migration
  def change
    add_column :types, :category_id, :int
  end
end
