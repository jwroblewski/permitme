# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
puts 'Creating roles'
Role.create([
  { :name => 'admin' },
  { :name => 'user' },
  { :name => 'VIP' }
], :without_protection => true)

puts "Setting up default user"

user = User.create! :name=>"James", :email => "wroblewski.james@gmail.com", :password=>"changeme", :password_confirmation => "changeme"
puts "User created: " << user.name
user.add_role :admin

puts user.name << "'s permissions: "
user.roles.each do |role|
  puts "\t" << role.name
end
puts "Setting up some categories"
Category.create([
  { :name => 'Accomodations'},
  { :name => 'Administrative and Support Services'},
  { :name => "Food and Beverage"}
])

puts "Setting up some types"
Type.create([
  { :name =>"Restaurant"},
  { :name => "Bar"}
])

puts "Setting up some permits"
Permit.create([
  {
    :name => "Food Service Manager Certificate",
    :type_ids => [1,2],
    :city => "Houston"
  },
  {
    :name => "Food Dealer's Permit",
    :type_ids => [1,2],
    :city => "Houston"
  }
])
