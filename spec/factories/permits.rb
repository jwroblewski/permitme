# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :permit do
    name "MyString"
    description "MyString"
    string "MyString"
    url "MyString"
    type_id 1
    county "MyString"
    state "MyString"
    city "MyString"
    section 1
    resource_group_description "MyText"
  end
end
