Rails3BootstrapDeviseCancan::Application.routes.draw do
  resources :permits

  resources :types

  resources :categories

  authenticated :user do
    root :to => 'home#index'
  end
  root :to => "home#index"
  get 'about' => 'home#about'
  get 'contact'=>'home#contact'
  get 'news'=>'home#news'


  get 'starting' => 'home#starting', :as=>'starting'
  get 'running' => 'home#running', :as=>'running'

  #selected a business

  get 'starting/:type' => 'home#showStarting', :as=>'showStarting'
  get 'running/:type' => 'home#showRunning', :as=>'showRunning'
  get 'report' => 'home#report', :as=>'report'
  devise_for :users
  resources :users
end