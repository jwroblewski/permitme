class PermitsController < ApplicationController
  # GET /permits
  # GET /permits.json
  def index
    @permits = Permit.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @permits }
    end
  end

  # GET /permits/1
  # GET /permits/1.json
  def show
    @permit = Permit.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: [@permit, :types=>@permit.types ]}
    end
  end

  # GET /permits/new
  # GET /permits/new.json
  def new
    authorize! :new, @user, :message => 'Not authorized as an administrator.'
    @permit = Permit.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @permit }
    end
  end

  # GET /permits/1/edit
  def edit
    authorize! :edit, @user, :message => 'Not authorized as an administrator.'
    @permit = Permit.find(params[:id])
  end

  # POST /permits
  # POST /permits.json
  def create
    authorize! :create, @user, :message => 'Not authorized as an administrator.'
    @permit = Permit.new(params[:permit])

    respond_to do |format|
      if @permit.save
        format.html { redirect_to @permit, notice: 'Permit was successfully created.' }
        format.json { render json: @permit, status: :created, location: @permit }
      else
        format.html { render action: "new" }
        format.json { render json: @permit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /permits/1
  # PUT /permits/1.json
  def update
    authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @permit = Permit.find(params[:id])

    respond_to do |format|
      if @permit.update_attributes(params[:permit])
        format.html { redirect_to @permit, notice: 'Permit was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @permit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /permits/1
  # DELETE /permits/1.json
  def destroy
    authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    @permit = Permit.find(params[:id])
    @permit.destroy

    respond_to do |format|
      format.html { redirect_to permits_url }
      format.json { head :no_content }
    end
  end
end
