class HomeController < ApplicationController
  def index

  end
  def about

  end
  def contact

  end
  def news

  end
  #Starting or Running a business
  def starting
    @categories = Category.all
  end
  def showStarting
    type = Type.find_by_name(params[:type].capitalize)
    @permits = Permit.all(:include=>:types,
      :conditions=>["types.id=? AND permits.city=?", type, "Houston"]
    )
  end
  def running
    @categories = Category.all
  end
  def showRunning
    type = Type.find_by_name(params[:type].capitalize)
    @permits = Permit.all(:include=>:types,
      :conditions=>["types.id=? AND permits.city=?", type, "Houston"]
    )
  end
end
