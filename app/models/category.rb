class Category < ActiveRecord::Base
  attr_accessible :name
  has_many :types

  default_scope order('name ASC')
end
