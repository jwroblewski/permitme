class Type < ActiveRecord::Base
  attr_accessible :name, :category_id

  belongs_to :category
  has_and_belongs_to_many :permits
  validates_presence_of :category
end
