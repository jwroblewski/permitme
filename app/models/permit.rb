class Permit < ActiveRecord::Base
  attr_accessible :city, :county, :description, :name, :resource_group_description, :section, :state, :string, :type_ids, :url

  has_and_belongs_to_many :types
  validates_presence_of :types
end
